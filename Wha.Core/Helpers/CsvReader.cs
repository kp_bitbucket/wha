﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wha.Core.Helpers
{
    public static class CsvReader
    {
        public static IEnumerable<IList<string>> FromStream(Stream csv, bool ignoreFirstLine, Encoding encoding = null)
        {
            var reader = encoding != null
                ? new StreamReader(csv, encoding)
                : new StreamReader(csv);

            using (reader)
            {
                foreach (var item in FromReader(reader, ignoreFirstLine)) yield return item;
            }
        }

        public static IEnumerable<IList<string>> FromReader(StreamReader csv, bool ignoreFirstLine)
        {
            char delimiter;

            if (ignoreFirstLine) csv.ReadLine();

            IList<string> result = new List<string>();

            StringBuilder curValue = new StringBuilder();
            char c;
            c = (char)csv.Read();

            if (Equals(csv.CurrentEncoding, Encoding.UTF8))
            {
                delimiter = ',';
            }
            else if (Equals(csv.CurrentEncoding, Encoding.Unicode))
            {
                delimiter = '\t';
            }
            else
            {
                throw new NotSupportedException(csv.CurrentEncoding.EncodingName + " Encoding is not supported");
            }

            while ((int)c > 0 && !csv.EndOfStream)
            {
                Action handleDefault = () =>
                {
                    while ((int)c > 0 && c != delimiter && c != '\r' && c != '\n' && !csv.EndOfStream)
                    {
                        curValue.Append(c);
                        c = (char)csv.Read();
                    }
                    if ((int)c > 0 && c != delimiter && c != '\r' && c != '\n' && csv.EndOfStream)
                        curValue.Append(c);

                    result.Add(curValue.ToString());
                    curValue = new StringBuilder();
                    if (c == delimiter) c = (char)csv.Read(); //either ',', newline, or endofstream

                };

                switch (c)
                {
                    case ',': //empty field
                    case '\t':
                        if (delimiter == c)
                        {
                            result.Add("");
                            c = (char)csv.Read();
                        }
                        else
                        {
                            handleDefault();
                        }
                        break;

                    case '"': //qualified text
                    case '\'':
                        char q = c;
                        c = (char)csv.Read();
                        bool inQuotes = true;
                        while ((int)c > 0 && inQuotes && !csv.EndOfStream)
                        {
                            if (c == q)
                            {
                                c = (char)csv.Read();
                                if (c != q)
                                    inQuotes = false;
                            }

                            if (inQuotes)
                            {
                                curValue.Append(c);
                                c = (char)csv.Read();
                            }
                        }
                        result.Add(curValue.ToString());
                        curValue = new StringBuilder();
                        if (c == delimiter) c = (char)csv.Read(); // either ',', newline, or endofstream
                        break;
                    case '\n': //end of the record
                    case '\r':
                        if (result.Count > 0) // don't return empty records
                        {
                            yield return result;
                            result = new List<string>();
                        }
                        c = (char)csv.Read();
                        break;
                    default: //normal unqualified text
                        handleDefault();
                        break;
                }

            }
            if (curValue.Length > 0)
                result.Add(curValue.ToString());
            if (result.Count > 0)
                yield return result;

        }
        private static bool ignoreFirstLineDefault = false;
    }
}
