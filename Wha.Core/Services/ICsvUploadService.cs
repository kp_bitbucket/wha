﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wha.Core.Helpers;
using Wha.Framework;

namespace Wha.Core.Services
{
    public interface ICsvUploadService : IDependency
    {
        IEnumerable<IList<string>> StreamToList(Stream csv, bool ignoreFirstLine, Encoding encoding = null);
    }

    public class CsvUploadService : ICsvUploadService
    {
        public IEnumerable<IList<string>> StreamToList(Stream csv, bool ignoreFirstLine, Encoding encoding = null)
        {
            return CsvReader.FromStream(csv, ignoreFirstLine, null);
        }
    }

}
