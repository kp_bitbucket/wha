﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Wha.Framework.Caching
{
    public interface ICacheManager
    {
        TResult Get<TKey, TResult>(TKey key, Func<AcquireContext<TKey>, TResult> acquire);
        ICache<TKey, TResult> GetCache<TKey, TResult>();
    }
}
