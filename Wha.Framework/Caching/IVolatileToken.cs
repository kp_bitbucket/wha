﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wha.Framework.Caching
{
    public interface IVolatileToken
    {
        bool IsCurrent { get; }
    }
}