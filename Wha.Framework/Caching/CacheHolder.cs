﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Castle.DynamicProxy.Generators;

namespace Wha.Framework.Caching
{
    public class CacheHolder : ICacheHolder
    {
        private readonly ICacheContextAccessor _cacheContextAccesor;
        private readonly ConcurrentDictionary<CacheKey, object> _caches = new ConcurrentDictionary<CacheKey, object>();

        public CacheHolder(ICacheContextAccessor cacheContextAccessor)
        {
            _cacheContextAccesor = cacheContextAccessor;
        }

        class CacheKey : Tuple<Type, Type, Type>
        {
            public CacheKey(Type component, Type key, Type result)
                : base(component, key, result) { }
        }

        public ICache<TKey, TResult> GetCache<TKey, TResult>(Type component)
        {
            var cacheKey = new CacheKey(component, typeof(TKey), typeof(TResult));

            var result = _caches.GetOrAdd(cacheKey, k => new Cache<TKey, TResult>(_cacheContextAccesor));

            return (Cache<TKey, TResult>)result;
        }
    }
}
