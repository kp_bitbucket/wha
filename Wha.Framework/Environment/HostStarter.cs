﻿using Autofac;
using Wha.Framework.Caching;
using Wha.Framework.Environment.AutofacUtil;
using Wha.Framework.Settings;


namespace Wha.Framework.Environment
{
    public static class HostStarter
    {
        public static IContainer CreateHostContainer()
        {
            var builder = new ContainerBuilder();

            var assemblies = AutofacAssemblyStore.GetAssemblies();

            //startable
            builder.RegisterAssemblyTypes(assemblies)
                .Where(t => typeof(IStartable).IsAssignableFrom(t))
                .As<IStartable>()
                .SingleInstance();

            //modules
            builder.RegisterModule(new CacheModule());
            builder.RegisterModule(new SettingsModule());

            //cache
            builder.RegisterType<CacheHolder>().As<ICacheHolder>().SingleInstance();
            builder.RegisterType<CacheContextAccessor>().As<ICacheContextAccessor>().SingleInstance();
            builder.RegisterType<ParallelCacheContext>().As<IParallelCacheContext>().SingleInstance();
            builder.RegisterType<AsyncTokenProvider>().As<IAsyncTokenProvider>().SingleInstance();



            // assembly resolvers
            builder.RegisterType<DefaultAssemblyLoader>().As<IAssemblyLoader>().SingleInstance();
            builder.RegisterType<AppDomainAssemblyNameResolver>().As<IAssemblyNameResolver>().SingleInstance();
            builder.RegisterType<GacAssemblyNameResolver>().As<IAssemblyNameResolver>().SingleInstance();
            builder.RegisterType<FrameworkAssemblyNameResolver>().As<IAssemblyNameResolver>().SingleInstance();

            //
            builder.RegisterType<DefaultHost>().As<IHost>().SingleInstance().ExternallyOwned(); // manually dispose

            var container = builder.Build();

            return container;

        }

        private static void RegisterVolatileProvider<TRegister, TService>(ContainerBuilder builder) where TService : IVolatileProvider
        {
            builder.RegisterType<TRegister>()
                .As<TService>()
                .As<IVolatileProvider>()
                .SingleInstance();
        }

        public static IHost CreateHost()
        {
            var container = CreateHostContainer();
            return container.Resolve<IHost>();
        }
    }
}
