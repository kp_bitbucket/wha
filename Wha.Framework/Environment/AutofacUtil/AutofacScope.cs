﻿using System;

namespace Wha.Framework.Environment.AutofacUtil
{
    public static class AutofacScope
    {
        public const string Site = "SiteScope";
        public const string Work = "WorkScope";
    }
}
