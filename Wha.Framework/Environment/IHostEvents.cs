﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wha.Framework.Events;

namespace Wha.Framework.Environment
{
    public interface IHostEvents : IEventHandler
    {
        void Activated(ILifetimeScope container);

        void Terminating();
    }
}
