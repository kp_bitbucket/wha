﻿using Autofac;
using Autofac.Core;
using Autofac.Features.OwnedInstances;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wha.Framework.Environment.AutofacUtil;
using Autofac.Extras.DynamicProxy2;

namespace Wha.Framework.Environment
{
    public interface IHost : IDisposable
    {
        ILifetimeScope LifetimeScope { get; }
        void Activate();
        void Terminate();
        void BeginRequest();
        void EndRequest();
    }

    public class DefaultHost : IHost
    {
        private readonly ILifetimeScope _rootLifetimeScope;
        private readonly ILifetimeScope _siteLifetimeScope;
        private readonly Owned<IHostEvents> _hostEvents;

        private bool _disposed;
        public DefaultHost(ILifetimeScope rootLifetimeScope)
        {
            _rootLifetimeScope = rootLifetimeScope;

            var intermediateScope = _rootLifetimeScope.BeginLifetimeScope(builder =>
            {
                // assemblies
                var assemblies = AutofacAssemblyStore.GetAssemblies();

                // all modules: internal class module will NOT be picked up
                foreach (var item in assemblies.SelectMany(a => a.ExportedTypes).Where(t => typeof(IModule).IsAssignableFrom(t)))
                {
                    builder.RegisterType(item)
                        .As<IModule>()
                        .InstancePerDependency();
                }
            });

            _siteLifetimeScope = intermediateScope.BeginLifetimeScope(AutofacScope.Site, builder =>
            {
                // assemblies
                var assemblies = AutofacAssemblyStore.GetAssemblies();

                // modules
                foreach (var item in intermediateScope.Resolve<IEnumerable<IModule>>())
                {
                    builder.RegisterModule(item);
                }

                // dependencies
                builder.RegisterAssemblyTypes(assemblies)
                    .Where(t => typeof(IDependency).IsAssignableFrom(t))
                    .AsImplementedInterfaces()
                    .InstancePerLifetimeScope()
                    .EnableInterfaceInterceptors();

            });

            _hostEvents = _siteLifetimeScope.Resolve<Owned<IHostEvents>>();
        }
        public ILifetimeScope LifetimeScope
        {
            get { return _siteLifetimeScope; }
        }

        public void Activate()
        {
            _hostEvents.Value.Activated(_siteLifetimeScope);
        }

        public void Terminate()
        {
            //Optional logging here
            SafelyTerminate(() => { SafelyTerminate(() => _hostEvents.Value.Terminating()); });
            
            Dispose();
        }

        public void BeginRequest()
        {

        }

        public void EndRequest()
        {

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void SafelyTerminate(Action action)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                //Optional logging here
                throw e;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _siteLifetimeScope.Dispose();
                _rootLifetimeScope.Dispose();
            }
            _disposed = true;
        }
    }
}
