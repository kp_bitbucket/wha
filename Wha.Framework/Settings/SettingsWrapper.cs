﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Wha.Framework.Settings
{
    public class SettingsWrapper : NameValueCollection
    {

        public SettingsWrapper(NameValueCollection appSettings)
            : base(appSettings)
        {
        }

        public override string Get(string name)
        {
            if (name.EndsWith(".") || name.EndsWith("_"))
            {
                return base.Get(name);
            }

            if (AllKeys.Any(key => key.StartsWith(name, StringComparison.InvariantCultureIgnoreCase)))
            {
                var val = base.Get(name);
                if (!string.IsNullOrEmpty(val))
                {
                    return val;
                }
            }

            throw new KeyNotFoundException(string.Format("{0} - The given key was not present in the dictionary or the value for the key is not set.", name));
        }
    }
}
