﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Reflection;
using Wha.Framework.Environment;
using Wha.Framework.Environment.AutofacUtil;

namespace Wha
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IHost _host;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutofacAssemblyStore.LoadAssembly("Wha.Framework");
            AutofacAssemblyStore.LoadAssembly("Wha.Core");
            AutofacAssemblyStore.LoadAssembly(Assembly.GetExecutingAssembly());

            //create and activate host
            _host = HostStarter.CreateHost();
            _host.Activate();
        }
    }
}
