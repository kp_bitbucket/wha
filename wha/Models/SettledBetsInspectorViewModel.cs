﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wha.Models
{
    public class SettledBetsInspectorViewModel : CustomerRiskInspectorViewModel
    {
        private const decimal _unusualWinHistoryThreshold = 0.60M;

        public decimal Win { get; set; }
        public bool HasUnusualWinHistory { get; set; }
        public decimal UnusualWinHistoryThreshold { get { return _unusualWinHistoryThreshold; } }
    }

    public class SettledBetsInspectorViewModelMapper : Profile
    {
        public SettledBetsInspectorViewModelMapper()
        {

        }

        protected override void Configure()
        {
 	        CreateMap<IList<string>, SettledBetsInspectorViewModel>()
               .ForMember(dst => dst.Customer, opt => opt.MapFrom(src => src != null ? Int32.Parse(src[0]) : 0))
               .ForMember(dst => dst.Event, opt => opt.MapFrom(src => src != null ? Int32.Parse(src[1]) : 0))
               .ForMember(dst => dst.Participant, opt => opt.MapFrom(src => src != null ? Int32.Parse(src[2]) : 0))
               .ForMember(dst => dst.Stake, opt => opt.MapFrom(src => src != null ? Convert.ToDecimal(src[3]) : 0))
               .ForMember(dst => dst.Win, opt => opt.MapFrom(src => src != null ? Convert.ToDecimal(src[4]) : 0));
        }
    }
}