﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wha.Models
{
    public class UnsettledBetsInspectorViewModel : CustomerRiskInspectorViewModel
    {
        public decimal ToWin { get; set; }
        public bool HasUnusualWinHistory { get; set; }

        public bool HasUnusualStakeRate { get; set; }

        public bool HasVeryUnusualStakeRate { get; set; }

        public bool HasVeryRiskyWinAmount { get; set; }
    }

    public class UnsettledBetsInspectorViewModelMapper : Profile
    {
       public UnsettledBetsInspectorViewModelMapper()
       {

       }

       protected override void Configure()
       {
           CreateMap<IList<string>, UnsettledBetsInspectorViewModel>()
               .ForMember(dst => dst.Customer, opt => opt.MapFrom(src => src != null ? Int32.Parse(src[0]) : 0))
               .ForMember(dst => dst.Event, opt => opt.MapFrom(src => src != null ? Int32.Parse(src[1]) : 0))
               .ForMember(dst => dst.Participant, opt => opt.MapFrom(src => src != null ? Int32.Parse(src[2]) : 0))
               .ForMember(dst => dst.Stake, opt => opt.MapFrom(src => src != null ? Convert.ToDecimal(src[3]) : 0))
               .ForMember(dst => dst.ToWin, opt => opt.MapFrom(src => src != null ? Convert.ToDecimal(src[4]) : 0));
       }
    }
}