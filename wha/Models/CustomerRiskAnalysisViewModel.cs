﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wha.Models
{
    public class CustomerRiskAnalysisViewModel
    {
        public SettledBetsInspectorViewModel[] SettledBetsToInspect { get; set; }

        public UnsettledBetsInspectorViewModel[] UnsettledBetsToInspect { get; set; }
    }
}