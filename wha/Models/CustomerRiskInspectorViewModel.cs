﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wha.Models
{
    public class CustomerRiskInspectorViewModel
    {
        public int Customer { get; set; }
        public int Event { get; set; }
        public int Participant { get; set; }
        public decimal Stake { get; set; }
    }
}