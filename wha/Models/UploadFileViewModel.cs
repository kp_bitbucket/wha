﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Wha.Models.CustomValidators;

namespace Wha.Models
{
    public class UploadFileViewModel
    {
        [Required(ErrorMessage = "Please upload a valid csv file.")]
        [ValidateFile]
        public HttpPostedFileBase CsvFile { get; set; }

        public bool HasSettled { get; set; }
    }
}