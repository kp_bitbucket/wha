﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wha.Mapper
{
    public static class AutoMapperExtensions
    {
        public static T Resolve<T>(this ResolutionContext resolutionContext)
        {
            var type = typeof(T);
            return (T)resolutionContext.Options.ServiceCtor(type);
        }
    }
}