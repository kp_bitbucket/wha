﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wha.Core.Services;
using Wha.Framework.Caching;
using Wha.Models;

namespace Wha.Controllers
{
    public class HomeController : Controller
    {
        private ICsvUploadService _csvUploadService;
        private IMappingEngine _mapper;
        private ICacheManager _cacheManager;

        public HomeController(ICsvUploadService csvUploadService, IMappingEngine mapper, ICacheManager cacheManager)
        {
            _csvUploadService = csvUploadService;
            _mapper = mapper;
            _cacheManager = cacheManager;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CustomerRiskInspector()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CustomerRiskInspector(UploadFileViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var csv = _csvUploadService.StreamToList(model.CsvFile.InputStream, true);

                    if (model.HasSettled)
                    {

                        var settledBets = _mapper.Map<IEnumerable<IList<string>>, List<SettledBetsInspectorViewModel>>(csv);

                        var summary = settledBets.GroupBy(o => o.Customer)
                                        .Select(i => new
                                        {
                                            Customer = i.Key,
                                            WinCount = i.Where(o => o.Win > 0).Count(),
                                            BetCount = i.Count(),
                                            UnusualWinThreshold = i.First().UnusualWinHistoryThreshold,
                                            HasUnusualWinHistory = ((decimal)i.Where(o => o.Win > 0).Count() / (decimal)i.Count()) >= i.First().UnusualWinHistoryThreshold
                                        });

                        var results = from l in settledBets
                                      join s in summary on l.Customer equals s.Customer
                                      orderby l.Customer
                                      select new SettledBetsInspectorViewModel
                                      {
                                          Customer = l.Customer,
                                          Event = l.Event,
                                          Participant = l.Participant,
                                          Stake = l.Stake,
                                          HasUnusualWinHistory = s.HasUnusualWinHistory,
                                          Win = l.Win
                                      };

                        

                        //Archive customer history in cache
                        foreach(var item in summary)
                        {
                            var key = string.Format("customer_0{0}", item.Customer);
                            var archive = _cacheManager.Get(key, context =>
                            {
                                return results.Where(o => o.Customer == item.Customer).ToList();
                            });
                        }

                        // MemoryCache
                        return View("SettledBetsInspector", new CustomerRiskAnalysisViewModel { SettledBetsToInspect = results.ToArray() });
                    }
                    else
                    {

                        var unSettledBets = _mapper.Map<IEnumerable<IList<string>>, List<UnsettledBetsInspectorViewModel>>(csv);

                        var history = new List<SettledBetsInspectorViewModel>();

                        var customerIds = unSettledBets.Select(o => o.Customer).Distinct();

                        //NOTE: There is a bug on the cache module that needs to be fixed but the idea is the csv file will be joined with the history and the properties HasUnusualStakeRate, HasVeryUnusualStakeRate, etc. will be binded accordingly based on the threshold  defined. 
                        foreach(var id in customerIds)
                        {
                            var key = string.Format("customer_0{0}", id);

                            var archive = _cacheManager.Get(key, context => {
                                return new List<SettledBetsInspectorViewModel>();
                            });

                            history.AddRange(archive);
                        }

                        var withHistory = from l in unSettledBets
                                      join h in history on l.Customer equals h.Customer
                                      orderby l.Customer
                                      select new UnsettledBetsInspectorViewModel
                                      {
                                          Customer = l.Customer,
                                          Event = l.Event,
                                          Participant = l.Participant,
                                          Stake = l.Stake
                                      };
                            
                        //TODO: 

                        return View("UnsettledBetsInspector", new CustomerRiskAnalysisViewModel { UnsettledBetsToInspect = unSettledBets.ToArray() });

                    }
                }

                return View();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}