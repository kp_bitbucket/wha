﻿using Autofac;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Wha.Framework.Environment;

namespace Wha
{
    public partial class Startup
    {
        private static IHost _host;

        public void Configuration(IAppBuilder app)
        {

        }
    }

    public partial class Startup
    {
        private static HttpConfiguration _httpConfiguration;

        private const string CurrentTestScopeKey = "current_test_scope";

        public static HttpConfiguration HttpConfiguration
        {
            get { return _httpConfiguration ?? (_httpConfiguration = new HttpConfiguration());  }
        }

        public static void SetCurrentTestScope(ILifetimeScope scope)
        {
            HttpConfiguration.Properties[CurrentTestScopeKey] = scope;

        }

        public static ILifetimeScope GetCurrentTestScope()
        {
            if (_httpConfiguration.Properties.ContainsKey(CurrentTestScopeKey))
            {
                return HttpConfiguration.Properties[CurrentTestScopeKey] as ILifetimeScope;
            }
            return null;
        }
    }
}