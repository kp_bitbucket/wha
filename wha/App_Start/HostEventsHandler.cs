﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration;
using Wha.Framework.Environment;

namespace Wha.Handlers
{
    public class HostEventsHandler : IHostEvents
    {

        public void Activated(ILifetimeScope container)
        {
            // mvc
            if (HttpContext.Current != null) // Note: only enable mvc when hosting in IIS
            {
                DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            }
        }

        public void Terminating()
        {
            throw new NotImplementedException();
        }
    }
}