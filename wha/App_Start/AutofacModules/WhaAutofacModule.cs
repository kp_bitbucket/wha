﻿using System.Web;
using Autofac;
using Autofac.Extras.DynamicProxy2;
using Autofac.Integration.Mvc;
using Wha.Framework.Environment.AutofacUtil;

namespace Wha.AutofacModules
{
    public class WhaAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // assemblies
            var assemblies = AutofacAssemblyStore.GetAssemblies();

            
            // ------------------MVC------------------
            // REF: http://docs.autofac.org/en/latest/integration/mvc.html

            if (HttpContext.Current == null) return; // Note: only enable mvc when hosting in IIS

            // Register your MVC controllers.
            builder.RegisterControllers(assemblies).EnableClassInterceptors();

            // OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(assemblies);
            builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();
        }
    }
}